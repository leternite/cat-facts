	var ReactDOM = require('react-dom');
	var React = require('react');
	const ApiLink = "https://cat-fact.herokuapp.com/facts/random?animal_type=cat&amount=";

	function MyComponent(props) {
		const [error, setError] = React.useState(null);
		const [isLoaded, setIsLoaded] = React.useState(false);
		const [items, setItems] = React.useState([]);
		const [fulllist, setFulllist] = React.useState([]);

		const [number, setNumber] = React.useState(2);
		const [quantity, setQua] = React.useState(2);
		const [query, setQuery] = React.useState(null);
		const queryref = React.useRef(null);
				
		React.useEffect(() => {
			console.log(ApiLink+number);
			setIsLoaded(false);
			fetch(ApiLink+number)
			.then(res => res.json())
			.then(
				(result) => {
					setIsLoaded(true);
					setItems(result);
					setFulllist(result);
				},
				(error) => {
					setIsLoaded(true);
					setError(error);
				}
			)
		}, [quantity])

		const changeNum = (event) => setNumber(event.target.value);
		const submitNum =() => setQua(number);

		const changeQuery = (event) => setQuery(event.target.value);
		const searchText = () => {
			console.log(fulllist.length+" "+items.length);
			console.log(query);
			var filteredList = fulllist.filter(function(item){
				return item.text.toLowerCase().search(query.toLowerCase())!== -1;
			});
			setItems (filteredList);
		}
		const clearSearch = () => {
			setItems (fulllist);
			setQuery(null);
			queryref.current.value = null;
		}
				
		if (error) {
			return <div>Error: {error.message}</div>;
		} else if (!isLoaded) {
			return   <div class="Wrapper">
						<div class="RingSpinner" ></div>
						<div class="LoadingMessage">Loading...</div>
					</div>;
		} else { 
			return (
				<div>
					<table class="settings">
						<tr>
							<td width="30px"></td>
							<td width="270px">Number of facts</td>
							<td width="230px"></td>
							<td width="320px"></td>
						</tr>
						<tr>
							<td></td>
							<td><input type="number" min="2" max="50" value={number} onChange={changeNum} /></td>
							<td><button onClick={submitNum}> Show </button></td>
							<td></td>
						</tr>
						<tr>
							<td></td>
							<td>Find text in facts</td>
							<td></td>
							<td></td>
						</tr>
						<tr>
							<td></td>
							<td><input type="text" value={query} ref={queryref} onChange={changeQuery}/></td>
							<td><button onClick={searchText}> Search </button></td>
							<td><button onClick={clearSearch}> Show all </button></td>
						</tr>
					</table>
					<table class="facts">
						<tr><th width="30px">№</th><th width="270px">Fact</th><th width="120px">Animal</th><th width="220px">Time</th><th width="50px">Sent</th></tr>
						{items.map(item => {			
							var dateString = new Date(item.updatedAt);
							var updatetime = dateString.toLocaleString();
							var index = items.indexOf(item)+1;
						    return (<tr><td>{index}</td><td>{item.text}</td><td>{item.type}</td><td>{updatetime}</td><td>{item.status.sentCount}</td></tr>)
						})}					
					</table>
				</div>
				);
		}
	}

	ReactDOM.render(
		<MyComponent />,
		document.getElementById("app")
	)
